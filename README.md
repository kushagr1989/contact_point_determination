# README #

### What is this repository for? ###

* This repo contains C++ code that helps in estimation of contact point between object using the geometrical properties. Underlying software used to do so is "SWIFT++"
* This repo also contains C++ code that helps in visualisation of the contacts points found using SWIFT++ and render them in a virtual scene.
* Version 1.0

### How do I set everything up? ###

In order to get these programs up and running kindly follow the following steps.
* Following Dependencies need to be installed
1. Download and Install SWIFT++1.2 and QHULL-2012.1.
2. Clone and Install scl_manips from the following link
 https://bitbucket.org/samirmenon/scl-manips-v2

3. Clone the repo "contact_point_determination"and copy the folders named "Contact_Point_Detection" and "Graphics_Chai3d" into the swift++-1.2 folder.
4. Go into the Contact_Point_Detection folder and open the CMakeLists.txt file.
5. Change the path of QHULL_INC_DIR to point to the location where the "qhull_a.h" file lives.
6. Change the path in find library for QHULL LIBRARY to the location where the qhullstatic library lives.
7. Save and Open this location in terminal.
8. type sh make_rel.sh (this should generate the executable "contact_point_detection")
9. To see various options for running type ./contact_point_detection -h (help)
(If running by reading file (optitrack data) type ./contact_point_detection -p -n <No. Of lines in file> )
10. Now in order to render the contacts go into the Graphics_Chai3d folder and open the CMakeLists.txt
11. Change the path of "SCL_BASE_DIR" to point to where the scl_manips repo is residing on the machine.
12. Go into the code "disp_graphics.cpp" and change the string fname to point to the location where StanbotCfg.xml resides.
13. Save and Open this location in terminal.
14. type sh make_rel.sh (this should generate the executable "disp_graphics").
15. To run type ./disp_graphics on terminal.
### Running different models ###

1. Get the appropraite CAD model in .obj/.stl format.
2. Convert .obj/.stl to standard wireframe .obj format using online converter available at :
http://www.greentoken.de/onlineconv/
3. Convert .obj file to .ply file using PLY library available at : 
       http://www.cc.gatech.edu/projects/large_models/ply.html
Command (./obj2ply <(input file) >(output file))
4. Manually process the .ply file to make it compatible with SWIFT/decomposer i.e. .poly format. Following are the changes that need to be carried out
* Change ply -> POLY (first line)
* Delete all text upto the point where-after co-ordinates start, registering the number of vertexes and faces in the text to be deleted.
* After POLY, enter no. of vertexes and then number of faces. 
* Rename file as .poly
5. Use decomposer to make the file compatible for being used by SWIFT.
6. Import the object in SWIFT (by changing the filename ).

### Who do I talk to? ###

* Kushagr Gupta (kushagr@stanford.edu)
* Ellen Klingbeil
* Mohammed Khansari