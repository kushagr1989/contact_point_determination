//////////////////////////////////////////////////////////////////////////////
//
// contact_point_detection
//
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <sstream>

#include <SWIFT.h>

using std::cerr;
using std::endl;


// Initialize file for storing data
std::ofstream output("contact_point_data.txt", std::ofstream::out);

// Initialize file for reading data
std::ifstream input("output_data.txt", std::ifstream::in);
bool Enabled_opti=true;	// TRUE: If using optitrack and reading position and orientation from output_data.txt
std::string line;

// Object filename
static const char* object1_filename = "plane_SWIFT.poly";  // env object
static const char* object2_filename = "cylinder1.poly"; // tool object

static const SWIFT_Real vs1[] =
{
    2.0,0.0,0.0,
    3.0,0.0,0.0,
    3.0,1.0,0.0,
    2.0,1.0,0.0
};

static const int fs1[] =
{
    0,1,3,
    2,3,1,
    3,2,0,
    2,1,0
};

static const int vn1 = 4;
static const int fn1 = 4;	// Plane 1 (in XY plane-to test and verify )

static const SWIFT_Real vs2[] =
{
    0.0,0.0,0.0,
    1.0,0.0,0.0,
    1.0,1.0,0.0,
    0.0,1.0,0.0
};

static const int fs2[] =
{
    0,1,3,
    2,3,1,
    3,2,0,
    2,1,0
};

static const int vn2 = 4;
static const int fn2 = 4;	// Plane 2 (in XY plane-to test and verify )



// Command line options
int nsteps = 1;
SWIFT_Real avel = 1.0;
bool print = false;

// Transformations for object1
SWIFT_Real R_tool[9],R_env[9];
SWIFT_Real T_tool[3],T_env[3];
SWIFT_Real Q[4];    // Quaternion for the second object's rotation

int id1, id2,id3,id4;

// SWIFT scene
SWIFT_Scene* scene;

void Initialize_Scene( )
{
    // Create the swift scene with local bounding box sorting
    scene = new SWIFT_Scene( true, false );
    cerr << "Created a SWIFT_Scene" << endl;

    
    // Add an object described by arrays.  This model must be convex.
    if( !scene->Add_Convex_Object( vs1, fs1, vn1, fn1, id1, false ) ) {
        cerr << "Adding object1 failed -- Exiting..." << endl;
        exit( -1 );
    } else {
        cerr << "Added object1 to scene" << endl;
    }

    if( !scene->Add_Convex_Object( vs2, fs2, vn2, fn2, id2, false ) ) {
        cerr << "Adding object2 failed -- Exiting..." << endl;
        exit( -1 );
    } else {
        cerr << "Added object2 to scene" << endl;
    }
    if( !scene->Add_General_Object( object1_filename, id3, false ) ) {
        cerr << "Adding object3 failed -- Exiting..." << endl;
        exit( -1 );
    } else {
        cerr << "Added object3 to scene" << endl;
    }
     if( !scene->Add_General_Object( object2_filename, id4, false ) ) {
        cerr << "Adding object4 failed -- Exiting..." << endl;
        exit( -1 );
    } else {
        cerr << "Added object4 to scene" << endl;
    }
    // We wish to query the contacts between object3 & object4.
    // Deactivate all pairs
    scene->Deactivate();

    // Activate only pair of objects read from files
    scene->Activate( id3,id4 );
}

void Do_Steps( )
{
    int scount;
    double tm;		
    int np, i,j,k, temp=0;
    int* oids, *num_con, *ft_tp, *ft_id;
    SWIFT_Real* dists;
    SWIFT_Real* near_pt, *normal;
    SWIFT_Real* con_pt= new SWIFT_Real[3];
    SWIFT_Real* temp_pt= new SWIFT_Real[3];
    SWIFT_Real ang = 0.0;
    T_tool[0] = 0.0; // + cos(ang/50);
    T_tool[1] = 0.0;//sin(ang/50);
    T_tool[2] = 0.05;
	

   for( scount = 0; scount < nsteps; scount++ ) {

     if (Enabled_opti) {
	getline(input,line);
	std::istringstream(line,std::ios_base::in)>>tm>>T_tool[0]>>T_tool[1]>>T_tool[2]
						  >>R_tool[0]>>R_tool[1]>>R_tool[2]
						  >>R_tool[3]>>R_tool[4]>>R_tool[5]
						  >>R_tool[6]>>R_tool[7]>>R_tool[8]
						  >>T_env[0]>>T_env[1]>>T_env[2]
						  >>R_env[0]>>R_env[1]>>R_env[2]
						  >>R_env[3]>>R_env[4]>>R_env[5]
						  >>R_env[6]>>R_env[7]>>R_env[8]
						  >>con_pt[0]>>con_pt[1]>>con_pt[2];
       }
     else {					
	// Adjust the quaternion Hardcoded Rotation
	Q[0] = 0.0 * sin( ang * 0.5 );
	Q[1] = 0.0 * sin( ang * 0.5 ); 	
        Q[2] = 1.0 * sin( ang * 0.5 );
        Q[3] = cos( ang * 0.5 );
	//cerr<< "quaternion : " <<Q[0] <<"  " <<Q[1] <<"  " <<Q[2] <<"  " <<Q[3] <<"\n";

        // Convert the quaternion to a matrix
        R_tool[0] = 1.0 - 2.0 * Q[1] * Q[1] - 2.0 * Q[2] * Q[2];
        R_tool[1] = 2.0 * Q[0] * Q[1] - 2.0 * Q[3] * Q[2];
        R_tool[2] = 2.0 * Q[0] * Q[2] + 2.0 * Q[3] * Q[1];
        R_tool[3] = 2.0 * Q[0] * Q[1] + 2.0 * Q[3] * Q[2];
        R_tool[4] = 1.0 - 2.0 * Q[0] * Q[0] - 2.0 * Q[2] * Q[2];
        R_tool[5] = 2.0 * Q[1] * Q[2] - 2.0 * Q[3] * Q[0];
        R_tool[6] = 2.0 * Q[0] * Q[2] - 2.0 * Q[3] * Q[1];
        R_tool[7] = 2.0 * Q[1] * Q[2] + 2.0 * Q[3] * Q[0];
        R_tool[8] = 1.0 - 2.0 * Q[0] * Q[0] - 2.0 * Q[1] * Q[1];

        //Translation of object
        T_tool[0] += 0.0; // + cos(ang/50);
        T_tool[1] += 0.005;//sin(ang/50);
        T_tool[2] = 0.01;

    	// Object 3 positioned at origin
    	R_env[0] = 1.0; R_env[1] = 0.0; R_env[2] = 0.0;
    	R_env[3] = 0.0; R_env[4] = 1.0; R_env[5] = 0.0;
    	R_env[6] = 0.0; R_env[7] = 0.0; R_env[8] = 1.0;

	T_env[0] = 0.0; T_env[1] = 0.0; T_env[2] = 0.0;
      }
        // Move the objects to its new position
        scene->Set_Object_Transformation( id3, R_env, T_env );
        scene->Set_Object_Transformation( id4, R_tool, T_tool );
        
        // Query Contact Points
	scene->Query_Contact_Determination(false, 0.1, np, &oids, &num_con, &dists, &near_pt,&normal, &ft_tp, &ft_id );
        if( print ) { 

            cerr << "Distances at step " << scount+1 << ":" << endl;
	    if(np>0) {	
             for( i = 0; i < np; i++ ) {
                     cerr << "Object " << oids[i<<1] << " vs. Object "
                     << oids[(i<<1)+1] << " = " << dists[i] << "\nNo. of contacts are = "<< num_con[i] << endl;
		/************* FINDING THE FEATURE TYPES AND CONTACT NORMAL FOR THE OBJECTS UNDER STUDY *******/
               for (j=0;j<num_con[i];j++) {
		     cerr << "\nContact Normal : "<<normal[(i+j)*3+0] << "\t"<<normal[(i+j)*3+1] << "\t"<<normal[(i+j)*3+2];	
		     cerr << "\nfeature type " << ft_tp[(i+j)<<1] << " and " <<ft_tp[((i+j)<<1)+1];
		     
			if(ft_tp[(i+j)*2]==1) {
		  	 cerr << "\nVERTEX in Object "<< oids[i<<1] <<" and Vt. number " << ft_id[temp];
		  	 temp+=1;
		 	}			
		 	else if(ft_tp[(i+j)*2]==2) { 
  		 	 cerr << "\nEDGE in Object "<< oids[i<<1] <<
		 	 " between vertices number " << ft_id[temp] <<" & " <<ft_id[temp+1];
		 	 temp+=2;
			}
		 	else if(ft_tp[(i+j)*2]==3) { 
  		  	 cerr << "\nFACE in Object "<< oids[i<<1] <<" and face number " << ft_id[temp]; 
		         temp+=1;
			}
			if(ft_tp[(i+j)*2+1]==1) {
			 cerr << "\nVERTEX in Object "<< oids[(i<<1)+1] <<" and Vt. number " << ft_id[temp];
			 temp+=1; 
			}
			else if(ft_tp[(i+j)*2+1]==2) {
  			 cerr << "\nEDGE in Object "<< oids[(i<<1)+1] <<
			 " between vertices number " << ft_id[temp] <<" & " <<ft_id[temp+1];
			 temp+=2;
			}
	        	else if(ft_tp[(i+j)*2+1]==3) {
  			 cerr << "\nFACE in Object "<< oids[(i<<1)+1] <<" and face number " << ft_id[temp];
			 temp+=1;
			}
			/*if(ft_tp[(i+j)*2]==1)		Find length of feature ids...not reqd
				size+=1;
			else if(ft_tp[(i+j)*2]==2)
				size+=2;
			else if(ft_tp[(i+j)*2]==3)
				size+=1;
			if(ft_tp[(i+j)*2+1]==1)
				size+=1;
			else if(ft_tp[(i+j)*2+1]==2)
				size+=2;
			else if(ft_tp[(i+j)*2+1]==3)
				size+=1;*/
         /************* TRANSFORMING THE NEAREST POINTS FOR THE OBJECTS TRANSOFRMED *******/
			if(oids[i<<1]==2) // env object i.e. transformed (id(num)-1)
			      {
				temp_pt[0]=T_env[0]+R_env[0]*near_pt[(i+j)*6+0] + R_env[1]*near_pt[(i+j)*6+1] + R_env[2]*near_pt[(i+j)*6+2] ;
				temp_pt[1]=T_env[1]+R_env[3]*near_pt[(i+j)*6+0] + R_env[4]*near_pt[(i+j)*6+1] + R_env[5]*near_pt[(i+j)*6+2] ;
				temp_pt[2]=T_env[2]+R_env[6]*near_pt[(i+j)*6+0] + R_env[7]*near_pt[(i+j)*6+1] + R_env[8]*near_pt[(i+j)*6+2] ;
			        near_pt[(i+j)*6+0]=temp_pt[0];
			        near_pt[(i+j)*6+1]=temp_pt[1];
				near_pt[(i+j)*6+2]=temp_pt[2];
			      }
			   else if (oids[(i<<1)+1]==2)
 			     {
			        temp_pt[0]=T_env[0]+R_env[0]*near_pt[(i+j)*6+0] + R_env[1]*near_pt[(i+j)*6+1] + R_env[2]*near_pt[(i+j)*6+2] ;
				temp_pt[1]=T_env[1]+R_env[3]*near_pt[(i+j)*6+0] + R_env[4]*near_pt[(i+j)*6+1] + R_env[5]*near_pt[(i+j)*6+2] ;
				temp_pt[2]=T_env[2]+R_env[6]*near_pt[(i+j)*6+0] + R_env[7]*near_pt[(i+j)*6+1] + R_env[8]*near_pt[(i+j)*6+2] ;
			        near_pt[(i+j)*6+0]=temp_pt[0];
			        near_pt[(i+j)*6+1]=temp_pt[1];
				near_pt[(i+j)*6+2]=temp_pt[2];	   
			     }
			  if(oids[i<<1]==3) // tool object i.e. transformed (id(num)-1)
			      {
				temp_pt[0]=T_tool[0]+R_tool[0]*near_pt[(i+j)*6+0] + R_tool[1]*near_pt[(i+j)*6+1] + R_tool[2]*near_pt[(i+j)*6+2] ;
				temp_pt[1]=T_tool[1]+R_tool[3]*near_pt[(i+j)*6+0] + R_tool[4]*near_pt[(i+j)*6+1] + R_tool[5]*near_pt[(i+j)*6+2] ;
				temp_pt[2]=T_tool[2]+R_tool[6]*near_pt[(i+j)*6+0] + R_tool[7]*near_pt[(i+j)*6+1] + R_tool[8]*near_pt[(i+j)*6+2] ;
			        near_pt[(i+j)*6+0]=temp_pt[0];
			        near_pt[(i+j)*6+1]=temp_pt[1];
				near_pt[(i+j)*6+2]=temp_pt[2];
			      }
			   else if (oids[(i<<1)+1]==3)
 			     {
			        temp_pt[0]=T_tool[0]+R_tool[0]*near_pt[(i+j)*6+3] + R_tool[1]*near_pt[(i+j)*6+4] + R_tool[2]*near_pt[(i+j)*6+5] ;
				temp_pt[1]=T_tool[1]+R_tool[3]*near_pt[(i+j)*6+3] + R_tool[4]*near_pt[(i+j)*6+4] + R_tool[5]*near_pt[(i+j)*6+5] ;
				temp_pt[2]=T_tool[2]+R_tool[6]*near_pt[(i+j)*6+3] + R_tool[7]*near_pt[(i+j)*6+4] + R_tool[8]*near_pt[(i+j)*6+5] ;
			        near_pt[(i+j)*6+3]=temp_pt[0];
				near_pt[(i+j)*6+4]=temp_pt[1];
				near_pt[(i+j)*6+5]=temp_pt[2];		   
			     }
           /*************************DISPLAY THE POINTS ***********************/
			cerr << " \nNearest points are = \n";
			for(k=0;k<6;k++) {	   
			 cerr << near_pt[(i+j)*6+k]<< endl;	
			}
	         }
	      }
	   }
           else cerr<<"No contacts between any objects\n";
        }
	//Storing data in a file (used by chai3d program for rendering)
	output <<tm<<R_tool[0]<<"\t"<<R_tool[1]<<"\t"<<R_tool[2]<<"\t"
               <<R_tool[3]<<"\t"<<R_tool[4]<<"\t"<<R_tool[5]<<"\t"
               <<R_tool[6]<<"\t"<<R_tool[7]<<"\t"<<R_tool[8]<<"\t"
               <<T_tool[0]<<"\t"<<T_tool[1]<<"\t"<<T_tool[2]<<"\t"
	       <<R_env[0]<<"\t"<<R_env[1]<<"\t"<<R_env[2]<<"\t"
               <<R_env[3]<<"\t"<<R_env[4]<<"\t"<<R_env[5]<<"\t"
               <<R_env[6]<<"\t"<<R_env[7]<<"\t"<<R_env[8]<<"\t"
               <<T_env[0]<<"\t"<<T_env[1]<<"\t"<<T_env[2]<<"\t"
	       <<near_pt[0]<<"\t"<<near_pt[1]<<"\t"<<near_pt[2]<<"\t"
               <<near_pt[3]<<"\t"<<near_pt[4]<<"\t"<<near_pt[5]<<"\t"
	       <<con_pt[0]<<con_pt[1]<<con_pt[2]<<endl;   // 0,1,2 are are env object and 3,4,5 are for tool object
	ang=ang + 3.14/20;
    } // for loop for scount
 /*for (i=0; i< 12; i++) { //testing ids
 cerr<<normal[i] <<"\n";
}*/
}

void Print_Usage( )
{
    cerr << "Usage: " << endl;
    cerr << "  example [options]" << endl;
    cerr << "Options:" << endl;
    cerr << "  -h        : Print this help" << endl;
    cerr << "  -p        : Print the distances for each step" << endl;
    cerr << "  -n <int>  : Number of steps to run for" << endl;
    cerr << "  -a <real> : Angular velocity in degrees/step" << endl;
    cerr << "Default values: " << endl;
    cerr << "  n = 1"  << endl;
    cerr << "  a = 1.0"  << endl;
    cerr << "  print is false" << endl;
    exit( -1 );
}

int main( int argc, char **argv )
{
    int i;

    // Process command line options
    i = 1;
    while( i != argc ) {
        if( !strcmp( argv[i], "-h" ) ) {
            // Help
            Print_Usage();
        } else if( !strcmp( argv[i], "-p" ) ) {
            // Print results
            print = true;
        } else if( !strcmp( argv[i], "-n" ) ) {
            // Number of steps
            i++;
            nsteps = atoi( argv[i] );
            nsteps = nsteps <= 0 ? 1 : nsteps;
        } else if( !strcmp( argv[i], "-a" ) ) {
            // Angular velocity
            i++;
            avel = atof( argv[i] );
            avel = avel <= 0.0 ? 1.0 : avel;
        } else {
            break;
        }
        i++;
    }

    // Initialize the scenario
    Initialize_Scene();
    // Run the steps
    Do_Steps();
    input.close();
    output.close();	
    return 0;
}


