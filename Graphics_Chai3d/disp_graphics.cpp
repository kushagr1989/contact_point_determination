// Graphics

#include <scl/DataTypes.hpp>
#include <scl/Singletons.hpp>
#include <scl/data_structs/SGcModel.hpp>
#include <scl/dynamics/scl/CDynamicsScl.hpp>
#include <scl/dynamics/tao/CDynamicsTao.hpp>
#include <scl/parser/sclparser/CParserScl.hpp>
#include <scl/graphics/chai/CGraphicsChai.hpp>
#include <scl/graphics/chai/ChaiGlutHandlers.hpp>
#include <scl/control/task/CControllerMultiTask.hpp>
//For timing
#include <sutil/CSystemClock.hpp>

#include <fstream>
//Eigen 3rd party lib
#include <Eigen/Dense>
//Freeglut windowing environment
#include <GL/freeglut.h>
scl::SGraphicsParsed rgr; 
scl::CGraphicsChai rchai;  //Chai interface (updates graphics rendering tree etc.)
scl::CParserScl p;
bool flag;   

//Graphics Objects
chai3d::cGenericObject *goal_sphere1;
chai3d::cGenericObject *goal_sphere2;
chai3d::cGenericObject *goal_sphere3;



// Initialize file for reading data
std::ifstream input("../Contact_Point_Detection/contact_point_data.txt", std::ifstream::in);
std::string line;

int main( int argc, char **argv )
{

 /******************************ChaiGlut Graphics************************************/
   glutInit(&argc, argv); // We will use glut for the window pane (not the graphics).
	
   /*************************************GRAPHICS RENDERING INITIALIZATION****************************/ 
     const std::string fname("/home/kushagr_admin/Documents/scl-manips-v2.git/specs/Stanbot/StanbotCfg.xml");
     flag = p.readGraphicsFromFile(fname,"StanbotStdView",rgr);
     flag = flag && rchai.initGraphics(&rgr);
     flag = flag && scl_chai_glut_interface::initializeGlutForChai(&rgr, &rchai);
     if(false==flag) { std::cout<<"\nCouldn't initialize chai graphics\n"; return 1; }


    /******************************Add models to render************************************/
     flag = rchai.addMeshToRender("Object1","plane_SWIFT.obj",Eigen::Vector3d(0,0,0), Eigen::Matrix3d::Identity());
      if(false == flag){std::cout<<"\nCould not load object1"; return 1; }//env
     scl::SGraphicsChaiMesh *cap_mesh = rchai.getChaiData()->meshes_rendered_.at("Object1");

     flag = rchai.addMeshToRender("Object2","cylinder.obj",Eigen::Vector3d(0,0,0), Eigen::Matrix3d::Identity());
      if(false == flag){std::cout<<"\nCould not load object2"; return 1; }//tool
      scl::SGraphicsChaiMesh *plane_mesh = rchai.getChaiData()->meshes_rendered_.at("Object2");

       //Show Frames
      rchai.getChaiData()->meshes_rendered_.at("Object1")->graphics_obj_->setShowFrame(true);
      rchai.getChaiData()->meshes_rendered_.at("Object1")->graphics_obj_->setFrameSize(0.1);

      rchai.getChaiData()->meshes_rendered_.at("Object2")->graphics_obj_->setShowFrame(true);
      rchai.getChaiData()->meshes_rendered_.at("Object2")->graphics_obj_->setFrameSize(0.1);

      Eigen::Vector3d sphere_pos1(-0.5,-0.5,0.5);
      flag = rchai.addSphereToRender(sphere_pos1, goal_sphere1,0.03);
      if(false == flag){std::cout<<"\nCould not load the sphere in first object"; return 1; }

      Eigen::Vector3d sphere_pos2(-0.5,-0.5,0.5);
      flag = rchai.addSphereToRender(sphere_pos2, goal_sphere2,0.03);
      if(false == flag){std::cout<<"\nCould not load the sphere in second object"; return 1; }

      Eigen::Vector3d sphere_pos3(-0.5,-0.5,0.5); // render the force contact point
      flag = rchai.addSphereToRender(sphere_pos3, goal_sphere3,0.03);
      if(false == flag){std::cout<<"\nCould not load the sphere in second object"; return 1; }
      chai3d::cMaterial material;
      material.m_diffuse.setRed();
      material.m_emission.setRed();
      material.m_specular.setRed();
      material.m_ambient.setRed();
      goal_sphere3->setMaterial(material); 	

	while(true == scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running)
          { 
        glutMainLoopEvent(); const timespec ts = {0, 200000000};/*200ms*/ 
        //nanosleep(&ts,NULL);
        Eigen::Matrix3d R_tool,R_env;
	Eigen::Vector3d p_tool,p_env;
	double tm;
	Eigen::VectorXd Near_pt(6),con_pt_force(3);
        getline(input,line);
        std::istringstream(line,std::ios_base::in)>>tm>>R_tool(0,0)>>R_tool(0,1)>>R_tool(0,2)
					     >>R_tool(1,0)>>R_tool(1,1)>>R_tool(1,2)
					     >>R_tool(2,0)>>R_tool(2,1)>>R_tool(2,2)
					     >>p_tool(0)>>p_tool(1)>>p_tool(2)
					     >>R_env(0,0)>>R_env(0,1)>>R_env(0,2)
					     >>R_env(1,0)>>R_env(1,1)>>R_env(1,2)
					     >>R_env(2,0)>>R_env(2,1)>>R_env(2,2)
					     >>p_env(0)>>p_env(1)>>p_env(2)	
					     >>Near_pt(0)>>Near_pt(1)>>Near_pt(2)
					     >>Near_pt(3)>>Near_pt(4)>>Near_pt(5)
					     >>con_pt_force(0)>>con_pt_force(1)>>con_pt_force(2);

	chai3d::cMatrix3d rot_matrix_tool , rot_matrix_env,_90matrix;
        rot_matrix_tool.set(R_tool(0,0),R_tool(0,1),R_tool(0,2),R_tool(1,0),R_tool(1,1),R_tool(1,2),R_tool(2,0),R_tool(2,1),R_tool(2,2));
	rot_matrix_env.set(R_env(0,0),R_env(0,1),R_env(0,2),R_env(1,0),R_env(1,1),R_env(1,2),R_env(2,0),R_env(2,1),R_env(2,2));        
	_90matrix.set(1,0,0,0,0,1,0,-1,0);
        //rot_matrix=rot_matrix;//*_90matrix;
        //SET GOAL SPHERE POSITION
        goal_sphere1->setLocalPos(Near_pt(0),Near_pt(1),Near_pt(2));
	goal_sphere2->setLocalPos(Near_pt(3),Near_pt(4),Near_pt(5));
	goal_sphere3->setLocalPos(con_pt_force(0),con_pt_force(1),con_pt_force(2));

        rchai.getChaiData()->meshes_rendered_.at("Object2")->graphics_obj_->setLocalRot(rot_matrix_tool);	
        rchai.getChaiData()->meshes_rendered_.at("Object2")->graphics_obj_->setLocalPos(p_tool(0),p_tool(1),p_tool(2));

        rchai.getChaiData()->meshes_rendered_.at("Object1")->graphics_obj_->setLocalRot(rot_matrix_env);	
        rchai.getChaiData()->meshes_rendered_.at("Object1")->graphics_obj_->setLocalPos(p_env(0),p_env(1),p_env(2));
 	std::cout<<"\nPRESENT TIME IS :- "<<tm;

	}
       input.close();

}
